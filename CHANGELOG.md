# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.3] - (2022/10/02)
## Changes
- bumped dependencies
- added CI/CD pipeline

## [1.0.2] - (2022/10/02)
## Changes
- Updated readme

## [1.0.1] - (2022/10/02)
## Changes
- Fixed config schema
- Updated sample config

## [1.0.0] - (2022/10/02)
## Changes
- Initial published version
